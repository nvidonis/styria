from django import forms
from django.forms import ModelForm, CharField
from rssFeed.models import RssFeed

class RssFeedForm(ModelForm):
    class Meta:
        model = RssFeed
        fields = ['name', 'link']
        labels = {
            'name': 'Naslov',
            'link': 'Adresa'
        }
        prepopulated_fields = {'slug': ('name',)}



