# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-28 09:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RssFeed',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('link', models.URLField()),
                ('slug', models.SlugField(max_length=40, unique=True)),
            ],
        ),
    ]
