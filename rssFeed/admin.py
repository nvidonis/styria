from django.contrib import admin
from .models import RssFeed, RssEntry, Author

class RssFeedAdmin(admin.ModelAdmin):
    list_display = ("name", "link")
    prepopulated_fields = {"slug":("name",)}

class RssEntryAdmin(admin.ModelAdmin):
    list_display = ("title", "rss", "published")

class AuthorAdmin(admin.ModelAdmin):
    list_display = ("author_name",)


admin.site.register(RssFeed, RssFeedAdmin)
admin.site.register(RssEntry, RssEntryAdmin)
admin.site.register(Author, AuthorAdmin)