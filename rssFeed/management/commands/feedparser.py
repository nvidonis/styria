from django.core.management.base import BaseCommand, CommandError
from rssFeed.models import RssFeed, RssEntry, Author
import feedparser
from bs4 import BeautifulSoup
import datetime

class Command(BaseCommand):
    help = 'Retrieves entries and authors from RSS Feeds'

    def handle(self, *args, **options):
        rss = RssFeed.objects.all() #linkovi u bazi
        entries = RssEntry.objects.all() #članci u bazi
        authors_exist = Author.objects.all()
        
        #print(authors_exist)
        
        check_entries = []
        for entry in entries:
            check_entries.append(entry.entry_link)
        
        check_authors = []
        for author in authors_exist:
            check_authors.append(author.author_name)

        for x in range(0, len(rss)):
            #print(rss[x].link)
            
            feeds = feedparser.parse(rss[x].link) #budući članci

            #feeds.entries[x].title
            for i in range(0, len(feeds)):
                title = feeds.entries[i].title
                link = feeds.entries[i].link
                authors = feeds.entries[i].author
                published = feeds.entries[i].published_parsed

                published = datetime.datetime(*published[:6])
                        
                image = feeds.entries[i].summary_detail.value

                soup = BeautifulSoup(image, 'html.parser')

                
                if soup.img != None:
                    img_link = soup.img.get('src')

                elif feeds.entries[i].links != None:
                    image = feeds.entries[i].links

                    for a in range(0,len(image)):
                        if image[a].type == "image/jpeg" :
                            img_link = image[a].href
                else:
                    img_link = None
                
                author_list = authors.split(",") #autori ovog članka


                for n,i in enumerate(author_list):
                    if i.startswith("null"):
                        author_list[n]=i[5:]


                for author in author_list:
                    if author not in check_authors:
                        check_authors.append(author)
                        add_author = Author(author_name=author)
                        add_author.save()


                if link not in check_entries:
                    check_entries.append(link)
                    article = RssEntry(title=title, 
                        entry_link=link, 
                        #author=author, 
                        image_link=img_link, 
                        published=published, 
                        rss=RssFeed.objects.all()[x])
                    article.save()

                    for author in author_list:
                        author = Author.objects.get(author_name=author)
                        article.author_name.add(author)
                        article.save()

                    




