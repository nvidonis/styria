from django.db import models
from django.template.defaultfilters import slugify

class RssFeed(models.Model):
    name = models.CharField(max_length = 100)
    link = models.URLField(max_length=200)
    slug = models.SlugField(max_length=40, unique = True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(RssFeed, self).save(*args, **kwargs)

    def __str__(self):
        return self.name



class Author(models.Model):
    author_name = models.CharField(max_length=200)

    def __str__(self):
        return self.author_name


        
class RssEntry(models.Model):
    rss = models.ForeignKey(RssFeed, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    entry_link = models.URLField(max_length=200)
    author_name = models.ManyToManyField(Author)
    image_link = models.URLField(max_length=200, blank=True)
    published = models.CharField(max_length=200) #models.DateTimeField()

    def __str__(self):
        return self.title

    class Meta:
        ordering = ["-published"]


