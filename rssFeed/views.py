from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormView
from .models import RssFeed, RssEntry, Author
from rssFeed.forms import RssFeedForm
from django.core.paginator import Paginator

class FeedsFormView(ListView, FormView):
    """ index """
    template_name = 'rssFeed/index.html'
    context_object_name = 'rss_list'
    queryset = RssFeed.objects.all()

    form_class = RssFeedForm
    success_url = '/'

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.save()
        return super(FormView, self).form_valid(form)


class AllEntriesView(ListView):
    """ prikaz svih članaka bez obzira na feed """
    template_name = 'rssFeed/entries.html'
    context_object_name = 'entries'
    queryset = RssEntry.objects.all()
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(AllEntriesView, self).get_context_data(**kwargs)
        context['feeds'] = RssFeed.objects.all()
        return context


class FeedDetailView(DetailView):
    """ prikaz članaka po portalima """
    model = RssFeed
    template_name = 'rssFeed/entries.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(FeedDetailView, self).get_context_data(**kwargs)
        context['rss'] = self.object
        context['entries'] = RssEntry.objects.filter(rss=self.kwargs['pk'])
        context['feeds'] = RssFeed.objects.all()
    
        return context

    