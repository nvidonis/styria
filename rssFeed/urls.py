from django.conf.urls import url
from . import views
from rssFeed.views import FeedsFormView, AllEntriesView, FeedDetailView

urlpatterns = [
    url(r'^$', FeedsFormView.as_view()),
    url(r'^entry$', AllEntriesView.as_view()),
    url(r'^feed/(?P<pk>\d+)/$', FeedDetailView.as_view()),
]

