# README #

### Styria zadatak 2016 ###

* Napraviti stranicu s prikazom liste i formu za unos adrese i naslova RSS feeda.
* Napraviti Django manage komandu koja dohvaća aktivne feedove i sprema njihove unose 
(entries) u bazu.
* Napraviti stranicu s prikazom unosa obrnuto kronološkim redom.
* Omogućiti prikaz unosa samo za određeni feed.
* Napraviti stranicu za brzo pretraživanje unosa po autoru. 


### Requirements ###
* beautifulsoup4==4.4.1
* Django==1.9.4
* feedparser==5.2.1